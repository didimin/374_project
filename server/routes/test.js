var express = require("express");
var router = express.Router();

router.get("/", function(req, res, next) {
    res.send("Test Server");
});

module.exports = router;