import React from 'react';
import './Navbar.css';
import { Navbar, Nav } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

const Navigation = (props) => {
    console.log(props);
    return (
        <div className='primaryBar'>
        <Navbar bg="primary" variant="dark">
            <Navbar.Brand href="/">GitLab Project Manager</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/Home">Home</Nav.Link>
                    <Nav.Link href="/Cohost">Cohost</Nav.Link>
                    <Nav.Link href="/Project">Projects</Nav.Link>
                    <Nav.Link href="/Release">Release</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
        </div>
    )
}

export default withRouter(Navigation);